package com.suna.pest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class HomeController {

	private static final String STATE = "STATE";
	private static final String CODE = "AUTHORIZATION_CODE";
	private static final String GRUNT_TYPE = "authorization_code";
	private static final String PRODUCT_ID = "41ea1a91-b57e-4fe5-ae4b-8e3cd976c296";
	private static final String PRODUCT_SECRET = "xPd8ozFU7A6uFuUpRp58Oh0Kn";
	private final static String NEST_FIREBASE_URL = "https://developer-api.nest.com";

	// private final static String BASE_ACCESS_TOKEN_URL =
	// "https://api.home.nest.com/";
	private final static String BASE_AUTHORIZATION_URL = "https://home.nest.com/login/oauth2";
	// public final static String CLIENT_CODE_URL = BASE_AUTHORIZATION_URL +
	// "login/oauth2?client_id=%s&state=%s";
	// public final static String ACCESS_URL = BASE_ACCESS_TOKEN_URL
	// +
	// "oauth2/access_token?code=%s&client_id=%s&client_secret=%s&grant_type=authorization_code";

	// private static final String AUTHORIZATION_URL =
	// "https://home.nest.com/login/oauth2?client_id=" + PRODUCT_ID
	// + "&state=" + STATE;
	// private static final String ACCESS_TOKEN_URL =
	// "https://api.home.nest.com/oauth2/access_token?client_id="
	// + PRODUCT_ID + "&code=" + CODE + "&client_secret=" + PRODUCT_SECRET +
	// "&grant_type=" + GRUNT_TYPE;

	@RequestMapping(value = "/home", method = RequestMethod.POST)
	public String renderHomePage(final Model model, final RedirectAttributes redirectAttributes) {

		System.out.println("post");
		return "home";
	}
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String homePage() {
		return "home";
	}

	@RequestMapping("/authorization")
	public String authorization(final Model model, final RedirectAttributes redirectAttributes) {
		redirectAttributes.addAttribute("client_id", PRODUCT_ID);
		redirectAttributes.addAttribute("state", STATE);
		return "redirect:" + BASE_AUTHORIZATION_URL;
	}

}
